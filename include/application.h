#pragma once
/*
 * Name: Demetri Van Sickle
 * Date: 05/19/2024
 * Class: CS302
 * Assignment: Program 3
 * Description:
 *      Contains the class declarations for the DessertManager
 */
#include "234Tree.h"

class DessertManager
{
public:
    DessertManager();
    ~DessertManager();

    /*
     * Description:
     *      Starts the main DessertManager loop
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void start();

private:
    ttfTree desserts;

    /*
     * Description:
     *      Finds the total profit for a spesific flavor
     * 
     * Arguments:
     *      Flavor - flavor to find the profit of
     * 
     * Returns:
     *      float - the total profit for the given flavor
    */
    float getTotalProfitOfFlavor(Flavor flavorToFind) const;

    /*
     * Description:
     *      Finds and returns the most popular flavor
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      Flavor - the most popular flavor
    */
    Flavor findMostPopularFlavor() const;

     /*
     * Description:
     *      Converts the flavor to a string
     * 
     * Arguments:
     *      Flavor - the flavor
     * 
     * Returns:
     *      std::string - the string representation of the flavor
     */
    std::string flavorToString(Flavor flavor) const;

    /*
     * Description:
     *      Prompts user to select a dessert to add
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void addDessert();

    enum mainMenuOptions
    {
        ADD_DESSERT,
        GET_TOTAL_PROFIT,
        DISPLAY,
        CLEAR_LIST,
        EXIT,

        OPTION_COUNT
    };

};