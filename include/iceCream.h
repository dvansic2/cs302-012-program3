#pragma once
/*
 * Name: Demetri Van Sickle
 * Date: 05/14/2024
 * Class: CS302
 * Assignment: Program 3
 * Description:
 *       Contains the class declarations that make up the IceCreamDessert hierarchy
 */
#include <random>

// Enumeration types for the different types of desserts
enum Flavor
{
    VANILLA,
    CHOCOLATE,
    STRAWBERRY,
    COOKIES_AND_CREAM,

    FLAVOR_COUNT,
    INVALID
};

enum dessertType
{
    CONE,
    SANDWICH,
    CAKE,

    DESSERT_COUNT
};

enum ConeType
{
    SUGAR,
    WAFFLE,
    BOWL,

    CONE_COUNT
};

enum Size
{
    SMALL,
    MED,
    LARGE,

    SIZE_COUNT
};

enum Filling
{
    NONE,
    CHERRY,
    FUDGE,
    CARAMEL,

    FILLING_COUNT
};

enum CookieType
{
    CHOCOLATE_CHIP,
    OATMEAL,
    PEANUT_BUTTER,

    COOKIE_COUNT
};


//********************************************************************************
//BEGIN IceCreamDessert
//********************************************************************************
class IceCreamDessert
{
public:
    IceCreamDessert();
    virtual ~IceCreamDessert();

    // virtual functions that will be overridden by the derived classes, see them for more information
    virtual void customize(bool suppriseMe) = 0;
    virtual void display() const = 0;
    virtual void applyDiscount() = 0;

    /*
    * Description:
    *      Returns the price of the dessert
    * 
    * Arguments:
    *      None
    * 
    * Returns:
    *      float - the price of the dessert
    */
    float getPrice() const;

    /*
    * Description:
    *      Returns the flavor of the dessert
    * 
    * Arguments:
    *      None
    * 
    * Returns:
    *      Flavor - the flavor of the dessert
    */
    Flavor getFlavor() const;

protected:
    float price; //The price of the dessert
    bool discountApplied; //If a discount has been applied
    Flavor flavor; //The flavor of the dessert

    /*
     * Description:
     *      Converts the flavor to a string
     * 
     * Arguments:
     *      Flavor - the flavor
     * 
     * Returns:
     *      std::string - the string representation of the flavor
     */
    std::string flavorToString(Flavor flavor) const;

     /*
     * Description:
     *      Generates a random int between the given min and max
     * 
     * Arguments:
     *      const int - the minimum int
     *      const int - the maximum int
     * 
     * Returns:
     *      int - the random int (casted from an int)
    */
    int generateRandomInt(const int min, const int max);

private:
    std::mt19937 eng; //Mersenne Twister engine
    std::uniform_int_distribution<> distr; //Uniform distribution
};
//********************************************************************************
//END IceCreamDessert
//********************************************************************************


//********************************************************************************
//BEGIN Cone
//********************************************************************************
class Cone : public IceCreamDessert
{
public:
    Cone();
    ~Cone();

    /*
     * Description:
     *      Allows the user to customize the cone
     * 
     * Arguments:
     *      bool - if true, the cone will be customized randomly
     * 
     * Returns:
     *      None
    */
    void customize(bool suppriseMe);

    /*
     * Description:
     *      Displays the cone
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void display() const;

    /*
     * Description:
     *      Applies a discount to the cone
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void applyDiscount();

private:
    ConeType coneType; //The type of cone
    int scoops; //The number of scoops

    /*
     * Description:
     *      Converts the cone type to a string
     * 
     * Arguments:
     *      ConeType type - the cone type
     * 
     * Returns:
     *      std::string - the string representation of the cone type
    */
    std::string coneTypeToString(ConeType type) const;

};
//********************************************************************************
//END Cone
//********************************************************************************


//********************************************************************************
//BEGIN Sandwich
//********************************************************************************
class Sandwich : public IceCreamDessert
{
public:
    Sandwich();
    ~Sandwich();

    /*
     * Description:
     *      Allows the user to customize the sandwich
     * 
     * Arguments:
     *      bool - if true, the sandwich will be customized randomly
     * 
     * Returns:
     *      None
    */
    void customize(bool suppriseMe);

    /*
     * Description:
     *      Displays the sandwich
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void display() const;

    /*
     * Description:
     *      Applies a discount to the sandwich
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void applyDiscount();

private:
    Size size; //The size of the sandwich
    CookieType cookie; //The type of cookie

    /*
     * Description:
     *      Converts the size to a string
     * 
     * Arguments:
     *      Size - the size
     * 
     * Returns:
     *      std::string - the string representation of the size
     */
    std::string sizeToString(Size size) const;

    /*
     * Description:
     *      Converts the cookie type to a string
     * 
     * Arguments:
     *      CookieType - the cookie type
     * 
     * Returns:
     *      std::string - the string representation of the cookie type
     */
    std::string cookieTypeToString(CookieType type) const;
};
//********************************************************************************
//END Sandwich
//********************************************************************************


//********************************************************************************
//BEGIN Cake
//********************************************************************************
class Cake : public IceCreamDessert
{
public:
    Cake();
    ~Cake();

    /*
     * Description:
     *      Allows the user to customize the cake
     * 
     * Arguments:
     *      bool - if true, the cake will be customized randomly
     * 
     * Returns:
     *      None
    */
    void customize(bool suppriseMe);

    /*
     * Description:
     *      Displays the cake
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void display() const;

    /*
     * Description:
     *      Applies a discount to the cake
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void applyDiscount();

    /*
     * Description:
     *      Writes a message on the cake
     * 
     * Arguments:
     *      std::string message - the message to write on the cake
     * 
     * Returns:
     *      None
    */
    void writeMessage(const std::string & message);

private:
    std::string message; //The message on the cake
    Filling filling; //The filling type

    /*
     * Description:
     *      Converts the filling type to a string
     * 
     * Arguments:
     *      Filling - the filling type
     * 
     * Returns:
     *      std::string - the string representation of the filling type
    */
    std::string fillingToString(Filling filling) const;
};
//********************************************************************************
//END Cake
//********************************************************************************