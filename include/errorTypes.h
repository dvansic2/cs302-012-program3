#pragma once
/*
 * Name: Demetri Van Sickle
 * Date: 05/14/2024
 * Class: CS302
 * Assignment: Program 3
 * Description:
 *      Contains error type definitions
 */

typedef struct
{
    int stringSize; //The size of the string that was passed in
} emptyString_t;

typedef struct 
{ 
} nullData_t;

typedef struct
{
    int size; //The size of the list
} emptyList_t; 

typedef struct
{
    int actualSize; //Size the list was
    int expectedSize; //Size the list should be
} invalidListSize_t;

typedef struct
{
} invalidInsert_t;

typedef struct
{
    int givenIndex; //Size the list was
    int maxIndex; //Size the list should be
} invalidIndex_t;