#pragma once
/*
 * Name: Demetri Van Sickle
 * Date: 05/15/2024
 * Class: CS302
 * Assignment: Program 3
 * Description:
 *      Contains the class declarations for the 234Tree
 */
#include <list>
#include <vector>
#include <memory>
#include "iceCream.h"

typedef std::shared_ptr< std::list< std::shared_ptr<IceCreamDessert> > > keyListshrdPtr;

enum keyPostions
{
    LEFT,
    MIDDLE,
    RIGHT
};

enum childPositions
{
    FAR_LEFT,
    MIDDLE_LEFT,
    MIDDLE_RIGHT,
    FAR_RIGHT
};

//********************************************************************************
//BEGIN Node
//********************************************************************************
class Node
{
public:
    Node();
    ~Node();

    Node(const Node & nodeToCopy);

    /*
     * Description:
     *      Returns true if the node has 3 keys
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      true - The node has 3 keys
     *      false - The node does not have 3 keys
     */
    bool isFull() const;

    /*
     * Description:
     *      Adds a list of dup keys to the node
     *      Will place the dup key list in the proper spot, shifting if needed
     *      Will return a pointer if the dup key list should go into a child
     * 
     * Arguments:
     *      keyListshrdPtr - The dup key list to add
     *      int & - what spot the key was placed at (0-3 if placed, -1 if not)
     * 
     * Returns:
     *      nullptr - no action
     *      std::shared_ptr<Node> - node that needs to go to next
    */
    std::shared_ptr<Node> addKey(keyListshrdPtr dupKeyList, int & keySpot);

    /*
     * Description:
     *      Prints out the node
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void display() const;

    /*
     * Description:
     *      Prints out the given key spot of
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void display(keyPostions position) const;

    /*
     * Description:
     *      Adds given given child pointer to the given index
     * 
     * Arguments:
     *      std::shared_ptr<Node> - shared pointer to child
     *      int - index at which to insert
     * 
     * Returns:
     *      None
    */
    void addChild(std::shared_ptr<Node> childPtr, int index);

    /*
     * Description:
     *      Adjust the child pointers to account for the key inserted at the given spot
     *      Used on the parent node when a child key moves back up due to a split
     * 
     * Arguments:
     *      std::shared_ptr<Node> - pointer to left node resulting from the split
     *      std::shared_ptr<Node> - pointer to right node resulting from the split
     *      int - spot where the child key was inserted
     * 
     * Returns:
     *      None
    */
    void adjustChildren(std::shared_ptr<Node> leftPtr, std::shared_ptr<Node> rightPtr, int spot);

    /*
     * Description:
     *      Returns the child at the given index
     *      NOTE: moves ownership of the pointer
     * 
     * Arguments:
     *      int - the index to find retreive the child
     *      bool - whether to transfer ownership (defaults to true)
     * 
     * Returns:
     *      std::shared_ptr<Node> - complete ownership of the child pointer
    */
    std::shared_ptr<Node> getChildAt(int index, bool transferOwnership);

    /*
     * Description:
     *      Returns the key at the given index
     *      NOTE: moves ownership of the pointer
     * 
     * Arguments:
     *      int - the index to find retreive the key
     *      bool - whether to transfer ownership (defaults to true)
     * 
     * Returns:
     *      keyListshrdPtr - complete ownership of the key pointer (which is a list)
    */
    keyListshrdPtr getKeyAt(int index, bool transferOwnership);

private:
    std::vector< keyListshrdPtr > keys; //Holds the keys of the node (to allow duplicates, it holds a list pointer)
    std::vector< std::shared_ptr<Node> > children; //Holds the child pointers

    /*
     * Description:
     *      Adds a list of dup keys to the node
     *      Will place the dup key list in the proper spot, shifting if needed
     *      Will return a pointer if the dup key list should go into a child
     * 
     * Arguments:
     *      int - current index of the keys vector
     *      keyListshrdPtr - The dup key list to add
     *      int & - what spot the key was placed at (0-3 if placed, -1 if not)
     * 
     * Returns:
     *      nullptr - no action
     *      std::shared_ptr<Node> - node that needs to go to next
    */
    std::shared_ptr<Node> addKeyR(int & curIndex, keyListshrdPtr dupKeyListToPlace, int & keySpot);

};
//********************************************************************************
//END Node
//********************************************************************************


//********************************************************************************
//BEGIN ttfTree
//********************************************************************************
class ttfTree
{
public:
    ttfTree();
    ~ttfTree();

    ttfTree(const ttfTree & treeToCopy);

    /*
     * Description:
     *      Inserts an item into the tree
     * 
     * Arguments:
     *      std::shared_ptr<IceCreamDessert> - shared pointer to insert
     * 
     * Returns:
     *      None
    */
    void insert(std::shared_ptr<IceCreamDessert> key);
    
    /*
     * Description:
     *      Display in order the tree
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void display() const;

    /*
     * Description:
     *      Finds and returns all the deserts of a given flavor
     * 
     * Arguments:
     *      Flavor - the flavor to find
     * 
     * Returns:
     *      keyListshrdPtr - shared pointer to list of desserts of a spesific flavor
    */
    keyListshrdPtr getDessertByFlavor(Flavor flavorToFind) const;

    /*
     * Description:
     *      Removes all the items from the tree
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void removeAll();

private:
    std::shared_ptr<Node> root; //Tree root pointer

    /*
     * Description:
     *      Inserts an item into the tree recursively
     * 
     * Arguments:
     *      std::shared_ptr<IceCreamDessert> - shared pointer to insert
     *      std::shared_ptr<Node> - the current node
     *      std::shared_ptr<Node> - the previous node
     * 
     * Returns:
     *      None
    */
    void insertR(keyListshrdPtr dupKeyListToPlace, std::shared_ptr<Node> currentNode, std::shared_ptr<Node> prevNode);

    /*
     * Description:
     *      Display in order the tree
     * 
     * Arguments:
     *      std::shared_ptr<Node> - the current node
     * 
     * Returns:
     *      None
    */
    void displayR(std::shared_ptr<Node> currentNode) const;

    /*
     * Description:
     *      Finds and returns all the deserts of a given flavor
     * 
     * Arguments:
     *      Flavor - the flavor to find
     *      std::shared_ptr<Node> - currentNode
     * 
     * Returns:
     *      keyListshrdPtr - shared pointer to list of desserts of a spesific flavor
    */
    keyListshrdPtr getDessertByFlavorR(Flavor flavorToFind, std::shared_ptr<Node> currentNode) const;

    /*
     * Description:
     *      Recursively copies the tree
     * 
     * Arguments:
     *      std::shared_ptr<Node> - the current node to copy
     *      std::shared_ptr<Node> - the previous local node
     * 
     * Returns:
     *      None
    */
    void copyR(std::shared_ptr<Node> currentNodeToCopy, childPositions & curPos, std::shared_ptr<Node> prevLocalNode); 
};


//********************************************************************************
//END ttfTree
//********************************************************************************