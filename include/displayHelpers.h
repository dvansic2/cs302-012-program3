    #pragma once
    /*
     * Name: Demetri Van Sickle
     * Date: 05/14/2024
     * Class: CS302
     * Assignment: Program 3
     * Description:
     *      Definitions for the display helper functions
     */

    /*
     * Description:
     *      Clears the screen
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
     */
    void clearScreen();

    /*
     * Description:
     *      Waits for the user to press enter
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
     */
    void waitForEnter();

     /*
     * Description:
     *      Converts string to int and validates a number option
     *      Used for validating user input for menu options
     * 
     * Arguments:
     *      const std::string & - The user input
     *      int - The max number of digits
     * 
     * Returns:
     *      int - The number
     */
    int validateNumOption(const std::string & userInput, const int maxDigits);