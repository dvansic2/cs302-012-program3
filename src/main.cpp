/*
 * Name: Demetri Van Sickle
 * Date: 05/20/2024
 * Class: CS302
 * Assignment: Program 3
 * Description:
 *      Main for program 3
 */
#include "application.h"

int main()
{
    DessertManager manager;
    manager.start();

    return 0;
}