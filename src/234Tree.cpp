/*
 * Name: Demetri Van Sickle
 * Date: 05/15/2024
 * Class: CS302
 * Assignment: Program 3
 * Description:
 *      Contains the class implementations for the 234Tree
 */
#include <iostream>
#include "234Tree.h"
#include "errorTypes.h"

#define MAX_KEYS 3
#define MAX_CHILDREN 4

//********************************************************************************
//BEGIN Node
//********************************************************************************
Node::Node()
{
    //Initialize the keys and children vectors
    keys.resize(MAX_KEYS + 1, nullptr);
    children.resize(MAX_CHILDREN, nullptr);
}

Node::~Node()
{
    //Not needed, using smart pointers
}

Node::Node(const Node & nodeToCopy)
{
    //Initialize the keys and children vectors
    keys.resize(MAX_KEYS + 1, nullptr);
    children.resize(MAX_CHILDREN, nullptr);

    //Cycle through the keys and copy them
    for(int position = 0; position < MAX_KEYS; ++position)
    {   
        if(nodeToCopy.keys[position] != nullptr)
        {   
            //Copy the list
            keys[position] = std::make_shared<std::list< std::shared_ptr<IceCreamDessert> > >(*nodeToCopy.keys[position]);
        }
    }
}
    
bool Node::isFull() const
{   
    //We can determine if we are full if the last key "slot" is occupied
    if(keys[MAX_KEYS - 1] == nullptr)
    {
        return false;
    }
    else
    {
        return true;
    }
}

void Node::display() const
{
    if(keys[0] == nullptr)
    {
        std::cout << "Node is empty" << std::endl;
        return;
    }

    std::cout << "\n\n----Keys------" << std::endl;
    for(std::shared_ptr< std::list< std::shared_ptr<IceCreamDessert> > > ptr : keys)
    {
        if(ptr == nullptr)
        {
            std::cout << "NULL" << std::endl;
        }
        else
        {
            if(ptr->size() > 1)
            {
                std::cout << "List contains " << ptr->size() << " desserts of flavor " << ptr->front()->getFlavor() << std::endl;
                std::cout << "First item is: " << std::endl;
            }
            //Only dislay the first item in the list for now
            ptr->front()->display();
        }
        std::cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-" << std::endl;
    }

    std::cout << "\n\n----Children------" << std::endl;
    for(std::shared_ptr<Node> ptr: children)
    {
        std::cout << ptr.get() << ", ";
    }
}

void Node::display(keyPostions position) const
{
    if(position < LEFT || position > RIGHT)
    {
        invalidIndex_t error;
        error.givenIndex = position;
        error.maxIndex = RIGHT;
        throw error;
    }

    if(keys[position] == nullptr)
    {
        return;
    }

    std::cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-" << std::endl;
    for(std::shared_ptr<IceCreamDessert> ptr : *keys[position])
    {
        ptr->display();
    }
    std::cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-" << std::endl;
}

std::shared_ptr<Node> Node::addKey(keyListshrdPtr dupKeyList, int & keySpot)
{
    int index = 0; //Index of the keys vector

    //If there are no keys in the node
    if(keys[0] == nullptr)
    {   
        //Assign that list to the first spot of the key vector
        keys[0] = std::move(dupKeyList);

        //Record the spot we placed at
        keySpot = 0;

        return nullptr;
    }

    return addKeyR(index, dupKeyList, keySpot);
}

void Node::addChild(std::shared_ptr<Node> childPtr, int index)
{   
    //Throw exception on invalid index
    if(index >= MAX_CHILDREN || index < 0)
    {
        invalidIndex_t error;
        error.givenIndex = index;
        error.maxIndex = MAX_CHILDREN;
        throw error;
    }

    children[index] = std::move(childPtr);
}

std::shared_ptr<Node> Node::getChildAt(int index, bool transferOwnership = true)
{
    //Throw exception on invalid index
    if(index >= MAX_CHILDREN || index < 0)
    {
        invalidIndex_t error;
        error.givenIndex = index;
        error.maxIndex = MAX_CHILDREN;
        throw error;
    }

    if(transferOwnership)
    {
        return std::move(children[index]);
    }
    else
    {
        return children[index];
    }
    
}

void Node::adjustChildren(std::shared_ptr<Node> leftPtr, std::shared_ptr<Node> rightPtr, int spot)
{
    switch(spot)
    {
    case LEFT:
        //Insert the left pointer at index 0, moving over the other pointers
        children.insert(children.begin(), std::move(leftPtr));

        //Reassign the pointer at index 1
        children[1] = std::move(rightPtr);
        break;
    
    case MIDDLE:
        //Insert the left pointer at index 1, moving over the other pointers
        children.insert(children.begin() + 1, std::move(leftPtr));

        //Reassign the pointer at index 2
        children[2] = std::move(rightPtr);
        break;

    case RIGHT:
        //Reassign the pointer at index 3
        children[2] = std::move(leftPtr);

        //Reassign the pointer at index 3
        children[3] = std::move(rightPtr);
        break;
    
    default:
        invalidIndex_t error;
        error.givenIndex = spot;
        error.maxIndex = RIGHT;
        throw error;
    }
}

keyListshrdPtr Node::getKeyAt(int index, bool transferOwnership = true)
{
    //Throw exception on invalid index
    if(index >= MAX_KEYS || index < 0)
    {
        invalidIndex_t error;
        error.givenIndex = index;
        error.maxIndex = MAX_CHILDREN;
        throw error;
    }

    if(transferOwnership)
    {
        return std::move(keys[index]);
    }
    else
    {
        return keys[index];
    }
    
}

std::shared_ptr<Node> Node::addKeyR(int & curIndex, keyListshrdPtr dupKeyListToPlace, int & keySpot)
{   
    Flavor listToPlaceFlavor; //Holds the flavor of of the list
    Flavor curKeyFlavor; //Holds the flavor of the current key
    Flavor nextKeyFlavor; //Holds the flavor of the next key

    //Base case
    if(keys[curIndex] == nullptr || curIndex == MAX_KEYS)
    {
        return nullptr;
    }

    //Store the current key's and list's flavor for easy access
    curKeyFlavor = keys[curIndex]->front()->getFlavor();
    listToPlaceFlavor = dupKeyListToPlace->front()->getFlavor();
    
    //******************************************************************
    // If the keyToPlace is equal to the current key
    //******************************************************************
    if(listToPlaceFlavor == curKeyFlavor)
    {
        //Assign it to the list
        keys[curIndex]->emplace_back(std::move(dupKeyListToPlace->front()));

        //Record the spot we placed at
        keySpot = curIndex;

        return nullptr;
    }

    //******************************************************************
    // If the keyToPlace is less than current key
    //******************************************************************
    if(listToPlaceFlavor < curKeyFlavor)
    {   
        //Check if there is a branch we can go down
        if(children[curIndex] != nullptr)
        {
            //Indicate we didnt place the key
            keySpot = -1;

            //Return the child 
            return children[curIndex];
        }

        //Insert at the current spot (will shift over others if necessary)
        keys.insert(keys.begin() + curIndex, std::move(dupKeyListToPlace));

        //Record the spot we placed at
        keySpot = curIndex;

        //Clean up any trailing nullptr elements 
        keys.resize(MAX_KEYS + 1);
        keys.shrink_to_fit();

        return nullptr;
    }


    //******************************************************************
    // If the keyToPlace is between current and next key
    //******************************************************************
    
    //If the next key isn't null
    if(keys[curIndex + 1] != nullptr)
    {
        //Store the next key's flavor for easy access
        nextKeyFlavor = keys[curIndex + 1]->front()->getFlavor();

        if(listToPlaceFlavor > curKeyFlavor && listToPlaceFlavor < nextKeyFlavor)
        {
            //Check if there is a branch we can go down
            if(children[curIndex + 1] != nullptr)
            {
                //Indicate we didnt place the key
                keySpot = -1;

                //Return the child 
                return children[curIndex + 1];
            } 

            //Insert at the next spot (will shift over others if necessary)
            keys.insert(keys.begin() + (curIndex + 1), std::move(dupKeyListToPlace));

            //Record the spot we placed at
            keySpot = curIndex + 1;

            //Clean up any trailing nullptr elements 
            keys.resize(MAX_KEYS + 1);
            keys.shrink_to_fit();

            return nullptr;
        }
    }
    //Next key is null
    else
    {
        if(listToPlaceFlavor > curKeyFlavor)
        {
            //Check if there is a branch we can go down
            if(children[curIndex + 1] != nullptr)
            {
                //Indicate we didnt place the key
                keySpot = -1;

                //Return the child 
                return children[curIndex + 1];
            }
            else if(curIndex == MAX_KEYS - 1)
            {
                //We should never be inserting into an ocupied 3rd curIndex
                throw invalidInsert_t{};
            }

            //Insert at the next spot
            keys.insert(keys.begin() + (curIndex + 1), std::move(dupKeyListToPlace));

            //Record the spot we placed at
            keySpot = curIndex + 1;

            //Clean up any trailing nullptr elements 
            keys.resize(MAX_KEYS + 1);
            keys.shrink_to_fit();

            return nullptr;
        }
    }

    //Continue to next key
    return addKeyR(++curIndex, dupKeyListToPlace, keySpot);
}

//********************************************************************************
//END Node
//********************************************************************************


//********************************************************************************
//BEGIN ttfTree
//********************************************************************************
ttfTree::ttfTree() : root(nullptr)
{
}

ttfTree::~ttfTree()
{
    //Not needed, using smart pointers
}

ttfTree::ttfTree(const ttfTree & treeToCopy) : root(nullptr)
{
    childPositions curPos = FAR_LEFT; //Current position in the node

    if(treeToCopy.root == nullptr)
    {
        return;
    }

    copyR(treeToCopy.root, curPos, root);
}

void ttfTree::display() const
{
    std::cout << "\n\nFull Tree:" << std::endl;

    displayR(root);
}

keyListshrdPtr ttfTree::getDessertByFlavor(Flavor flavorToFind) const
{
    return getDessertByFlavorR(flavorToFind, root);
}

void ttfTree::removeAll()
{
    root = nullptr; //Smart pointers will handle the rest
}

void ttfTree::insert(std::shared_ptr<IceCreamDessert> key)
{
    keyListshrdPtr newDupKeyList; //Holds pointer to new dup key list
    int keySpot; //Spot the key was placed in the node

    //Create a new dup key list
    newDupKeyList = std::make_shared<std::list< std::shared_ptr<IceCreamDessert> > >();
    newDupKeyList->emplace_front(std::move(key));

    //If root is null
    if(root == nullptr)
    {
        root = std::make_shared<Node>();
        root->addKey(std::move(newDupKeyList), keySpot);
        return;
    }

    insertR(std::move(newDupKeyList), root, root);
}

void ttfTree::insertR(keyListshrdPtr dupKeyListToPlace, std::shared_ptr<Node> currentNode, std::shared_ptr<Node> prevNode)
{
    std::shared_ptr<Node> childPtr; //Holds pointer to a given child
    std::shared_ptr<Node> newNodeLeft; //Holds new node
    std::shared_ptr<Node> newNodeMid; //Holds new node
    std::shared_ptr<Node> newNodeRight; //Holds new node
    int keySpot; //Spot the key was placed in the node

    //Base case
    if(currentNode == nullptr)
    {
        return;
    }

    //******************************************************************
    // If currentNode IS full
    //******************************************************************
    if(currentNode->isFull())
    {
        //Preform a split

        //If we have DONT have a parent
        if(currentNode == root)
        {
            //Create the new nodes
            newNodeLeft = std::make_shared<Node>();
            newNodeMid = std::make_shared<Node>();
            newNodeRight = std::make_shared<Node>();

            //Put the middle key into the new middle node
            newNodeMid->addKey(currentNode->getKeyAt(MIDDLE), keySpot);

            //Put the left value into the new left node
            newNodeLeft->addKey(currentNode->getKeyAt(LEFT), keySpot);

            //Put the right value into the new right node
            newNodeRight->addKey(currentNode->getKeyAt(RIGHT), keySpot);

            //Reassign the two left child pointers to the left node
            newNodeLeft->addChild(std::move(currentNode->getChildAt(FAR_LEFT)), FAR_LEFT);
            newNodeLeft->addChild(std::move(currentNode->getChildAt(MIDDLE_LEFT)), MIDDLE_LEFT);

            //Reassign the two right child pointers to the right node
            newNodeRight->addChild(std::move(currentNode->getChildAt(MIDDLE_RIGHT)), FAR_LEFT);
            newNodeRight->addChild(std::move(currentNode->getChildAt(FAR_RIGHT)), MIDDLE_LEFT);

            //Assign middle node's far left pointer to new left node
            newNodeMid->addChild(std::move(newNodeLeft), FAR_LEFT);

            //Assign middle node's middle left pointer to new right node
            newNodeMid->addChild(std::move(newNodeRight), MIDDLE_LEFT);

            //Reassign the root pointer
            root = newNodeMid; 

            //Reset the prevNode
            prevNode = root;
        }
        //If we DO have a parent
        else
        {
            //Create the new nodes
            newNodeLeft = std::make_shared<Node>();
            newNodeRight = std::make_shared<Node>();

            //Put the left value into the new left node
            newNodeLeft->addKey(currentNode->getKeyAt(LEFT), keySpot);

            //Put the right value into the new right node
            newNodeRight->addKey(currentNode->getKeyAt(RIGHT), keySpot);

            //Put the middle key into the parent node
            prevNode->addKey(currentNode->getKeyAt(MIDDLE), keySpot);

            //Reassign the two left child pointers to the left node
            newNodeLeft->addChild(std::move(currentNode->getChildAt(FAR_LEFT)), FAR_LEFT);
            newNodeLeft->addChild(std::move(currentNode->getChildAt(MIDDLE_LEFT)), MIDDLE_LEFT);

            //Reassign the two right child pointers to the right node
            newNodeRight->addChild(std::move(currentNode->getChildAt(MIDDLE_RIGHT)), FAR_LEFT);
            newNodeRight->addChild(std::move(currentNode->getChildAt(FAR_RIGHT)), MIDDLE_LEFT);

            //Adjust the child pointers of the parent
            prevNode->adjustChildren(std::move(newNodeLeft), std::move(newNodeRight), keySpot);
        }

        //Go back to the previus node and start there
        insertR(dupKeyListToPlace, prevNode, prevNode);
    }

    //******************************************************************
    // If currentNode is NOT full
    //******************************************************************
    else if(!currentNode->isFull())
    {   
        //Try to insert
        childPtr = currentNode->addKey(dupKeyListToPlace, keySpot);

        //Key was placed and we can exit
        if(childPtr == nullptr)
        {   
            return;
        }
        //We are told to look at a child
        else
        {
            insertR(dupKeyListToPlace, childPtr, currentNode);
        }
    }
}

void ttfTree::displayR(std::shared_ptr<Node> currentNode) const
{
    //Base case
    if(currentNode == nullptr)
    {
        return;
    }

    //Traverse all the left
    displayR(currentNode->getChildAt(FAR_LEFT, false));

    //Display the left value
    currentNode->display(LEFT);

    //Traverse all the middle left
    displayR(currentNode->getChildAt(MIDDLE_LEFT, false));

    //Display the middle value
    currentNode->display(MIDDLE);

    //Traverse all the middle right
    displayR(currentNode->getChildAt(MIDDLE_RIGHT, false));

    //Display the right value
    currentNode->display(RIGHT);

    //Traverse all the middle right
    displayR(currentNode->getChildAt(FAR_RIGHT, false));
}

keyListshrdPtr ttfTree::getDessertByFlavorR(Flavor flavorToFind, std::shared_ptr<Node> currentNode) const
{   
    keyListshrdPtr currDupKeyList; //Holds pointer to dup key list
    keyListshrdPtr nextDupKeyList; //Holds pointer to next dup key list

    //Base case
    if(currentNode == nullptr)
    {   
        //flavor is not in the tree
        return nullptr;
    }

    //******************************************************************
    // Look at LEFT key
    //******************************************************************
    //Get the current and next list
    currDupKeyList = currentNode->getKeyAt(LEFT, false);
    nextDupKeyList = currentNode->getKeyAt(MIDDLE, false);

    if(currDupKeyList != nullptr)
    {
        //If we are EQUAL
        if(currDupKeyList->front()->getFlavor() == flavorToFind)
        {
            return currDupKeyList;
        }

        //If we are LESS than
        if(flavorToFind < currDupKeyList->front()->getFlavor())
        {
            //Search the far left child
            return getDessertByFlavorR(flavorToFind, currentNode->getChildAt(FAR_LEFT, false));
        }

        //If we are BETWEEN the current and next key AND the next list is not NULL
        if(nextDupKeyList != nullptr && flavorToFind > currDupKeyList->front()->getFlavor() && flavorToFind < currDupKeyList->front()->getFlavor())
        {   
            //Search the middle left child
            return getDessertByFlavorR(flavorToFind, currentNode->getChildAt(MIDDLE_LEFT, false));
        }
        else if(nextDupKeyList == nullptr && flavorToFind > currDupKeyList->front()->getFlavor())
        {
            //Search the middle left child
            return getDessertByFlavorR(flavorToFind, currentNode->getChildAt(MIDDLE_LEFT, false));
        }
    }
    else
    {   
        //Left key should never be NULL
        throw emptyList_t {};
    }

    //******************************************************************
    // Look at MIDDLE key
    //******************************************************************
    //Get the current and next list
    currDupKeyList = currentNode->getKeyAt(MIDDLE, false);
    nextDupKeyList = currentNode->getKeyAt(RIGHT, false);

    if(currDupKeyList != nullptr)
    {
        //If we are EQUAL
        if(currDupKeyList->front()->getFlavor() == flavorToFind)
        {
            return currDupKeyList;
        }

        //If we are BETWEEN the current and next key AND the next list is not NULL
        if(nextDupKeyList != nullptr && flavorToFind > currDupKeyList->front()->getFlavor() && flavorToFind < currDupKeyList->front()->getFlavor())
        {   
            //Search the middle right child
            return getDessertByFlavorR(flavorToFind, currentNode->getChildAt(MIDDLE_RIGHT, false));
        }
        else if(nextDupKeyList == nullptr && flavorToFind > currDupKeyList->front()->getFlavor())
        {
            //Search the middle left child
            return getDessertByFlavorR(flavorToFind, currentNode->getChildAt(MIDDLE_RIGHT, false));
        }
    }
    else
    {   
        //flavor is not in the tree
        return nullptr;
    }

    //******************************************************************
    // Look at RIGHT key
    //******************************************************************
    //Get the current list
    currDupKeyList = currentNode->getKeyAt(MIDDLE, false);

    if(currDupKeyList != nullptr)
    {
        //If we are EQUAL
        if(currDupKeyList->front()->getFlavor() == flavorToFind)
        {
            return currDupKeyList;
        }

        //If we are GREATER than the current and next key AND the next list is not NULL
        if(flavorToFind > currDupKeyList->front()->getFlavor())
        {   
            //Search the far right child
            return getDessertByFlavorR(flavorToFind, currentNode->getChildAt(FAR_RIGHT, false));
        } 
    }

    //flavor is not in the tree
    return nullptr;
}

void ttfTree::copyR(std::shared_ptr<Node> currentNodeToCopy, childPositions & curPos, std::shared_ptr<Node> prevLocalNode)
{
    std::shared_ptr<Node> newLocalNode; //Holds pointer to new local node

    //Base case
    if(currentNodeToCopy == nullptr)
    {
        return;
    }

    //******************************************************************
    // Visit (copy) Node
    //******************************************************************
    if(root == nullptr)
    {
        root = std::make_shared<Node>(*currentNodeToCopy);
        newLocalNode = root;
    }
    else
    {   
        //Create new node
        newLocalNode = std::make_shared<Node>(*currentNodeToCopy);

        //Point our parent to us
        prevLocalNode->addChild(newLocalNode, curPos);
    }
    

    //******************************************************************
    // Visit FAR LEFT Child
    //******************************************************************
    curPos = FAR_LEFT;
    copyR(currentNodeToCopy->getChildAt(curPos, false), curPos, newLocalNode);

    //******************************************************************
    // Visit MIDLE LEFT Child
    //******************************************************************
    curPos = MIDDLE_LEFT;
    copyR(currentNodeToCopy->getChildAt(curPos, false), curPos, newLocalNode);

    //******************************************************************
    // Visit MIDLE RIGHT Child
    //******************************************************************
    curPos = MIDDLE_RIGHT;
    copyR(currentNodeToCopy->getChildAt(curPos, false), curPos, newLocalNode);

    //******************************************************************
    // Visit FAR RIGHT Child
    //******************************************************************
    curPos = FAR_RIGHT;
    copyR(currentNodeToCopy->getChildAt(curPos, false), curPos, newLocalNode);
}

//********************************************************************************
//END ttfTree
//********************************************************************************