/*
 * Name: Demetri Van Sickle
 * Date: 05/14/2024
 * Class: CS302
 * Assignment: Program 3
 * Description:
 *      Contains the implementations for the display helper functions
 */
#include <iostream>
#include <climits>
#include "displayHelpers.h"

void clearScreen()
{
    //Clear screen
    std::cout << "\033[2J\033[1;1H";
}

void waitForEnter()
{
    //Wait for user to press enter
    std::cout << "Press enter to continue..." << std::endl;
    std::cin.ignore(INT_MAX, '\n'); //This is ignoring all to the newline character, helps in cin.fails later in the program
    std::cin.get();
}

int validateNumOption(const std::string & userInput, const int maxDigits)
{
    //Make sure number of digits is valid
    if (static_cast<int>(userInput.length()) > maxDigits)
    {
        return INT_MIN;
    }

    //Make sure input is a number
    for(int i = 0; i < static_cast<int>(userInput.length()); ++i)
    {
        if (std::isdigit(userInput[i]) == false)
        {
            return INT_MIN;
        }
    }
    
    return std::stoi(userInput);
}
