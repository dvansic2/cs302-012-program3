/*
 * Name: Demetri Van Sickle
 * Date: 05/14/2024
 * Class: CS302
 * Assignment: Program 3
 * Description:
 *      Contains the class implementations for the IceCreamDessert hierarchy
 */

#include <iostream>
#include <climits>
#include "displayHelpers.h"
#include "iceCream.h"
#include "errorTypes.h"


#define DEFUALT_PRICE               5.99

#define CONE_MIN_SCOOPS             1
#define CONE_MAX_SCOOPS             5
#define CONE_DISCOUNT_PERCENT       0.10

#define SANDWICH_DISCOUNT_PERCENT   0.15

#define CAKE_DEFAULT_MESSAGE        "Happy Birthday!"
#define CAKE_DISCOUNT_PERCENT       0.20

//********************************************************************************
//BEGIN IceCreamDessert
//********************************************************************************
IceCreamDessert::IceCreamDessert() : price(DEFUALT_PRICE), discountApplied(false), flavor(VANILLA)
{
    std::random_device rd; //Seed for the random number generator
    eng = std::mt19937(rd()); //Initialize the engine with a random seed
    distr = std::uniform_int_distribution<>(-5, 5); //Distribution for the random number generator
}

IceCreamDessert::~IceCreamDessert()
{
}

float IceCreamDessert::getPrice() const
{
    return price;
}

Flavor IceCreamDessert::getFlavor() const
{
    return flavor;
}

std::string IceCreamDessert::flavorToString(Flavor flavor) const
{
    switch(flavor)
    {
        case VANILLA:
            return "Vanilla";
        case CHOCOLATE:
            return "Chocolate";
        case STRAWBERRY:
            return "Strawberry";
        case COOKIES_AND_CREAM:
            return "Cookies and Cream";
        default:
            return "Invalid";
    }
}

int IceCreamDessert::generateRandomInt(const int min, const int max)
{
    distr = std::uniform_int_distribution<>(min, max); //Distribution for the random number generator

    return distr(eng);
}

//********************************************************************************
//END IceCreamDessert
//********************************************************************************


//********************************************************************************
//BEGIN Cone
//********************************************************************************
Cone::Cone() : IceCreamDessert(), coneType(SUGAR), scoops(1)
{
}

Cone::~Cone()
{
}

void Cone::customize(bool suppriseMe)
{
    bool validInput = false; //Flag to determine if input is valid
    std::string userInput; //User input
    int userChoice; //User choice
    int diceRoll; //Random number for supprise me

    //If supprise me is selected, randomly generate the cone
    if(suppriseMe)
    {
        //Generate random cone type
        diceRoll = generateRandomInt(SUGAR, CONE_COUNT - 1);
        coneType = static_cast<ConeType>(diceRoll);

        //Generate random number of scoops
        diceRoll = generateRandomInt(CONE_MIN_SCOOPS, CONE_MAX_SCOOPS);
        scoops = diceRoll;

        //Generate random flavor
        diceRoll = generateRandomInt(VANILLA, FLAVOR_COUNT - 1);
        flavor = static_cast<Flavor>(diceRoll);
    }
    else //Customize cone
    {
        //Get participant type
        while (!validInput)
        {
            clearScreen();

            std::cout << "Pick the type of cone: " << std::endl;
            std::cout << "0. Sugar" << std::endl;
            std::cout << "1. Waffle" << std::endl;
            std::cout << "2. Bowl" << std::endl;
            
            try
            {
                //Get user input
                std::cin >> userInput;

                //Validate user input
                userChoice = validateNumOption(userInput, 1);

                if (userChoice == INT_MIN || userChoice < SUGAR || userChoice >= CONE_COUNT)
                {
                    throw std::invalid_argument("Invalid input");
                }
                
                //Set cone type
                coneType = static_cast<ConeType>(userChoice);

                clearScreen();

                //Get number of scoops
                std::cout << "Enter the number of scoops: ";
                std::cin >> userInput;

                //Validate user input
                userChoice = validateNumOption(userInput, 1);
                if (userChoice == INT_MIN || userChoice < CONE_MIN_SCOOPS || userChoice > CONE_MAX_SCOOPS)
                {
                    throw std::invalid_argument("Invalid input");
                }
                
                //Set number of scoops
                scoops = userChoice;

                clearScreen();

                //Get flavor
                std::cout << "Pick the flavor: " << std::endl;
                std::cout << "0. Vanilla" << std::endl;
                std::cout << "1. Chocolate" << std::endl;
                std::cout << "2. Strawberry" << std::endl;
                std::cout << "3. Cookies and Cream" << std::endl;

                //Get user input
                std::cin >> userInput;

                //Validate user input
                userChoice = validateNumOption(userInput, 1);
                if (userChoice == INT_MIN || userChoice < VANILLA || userChoice >= FLAVOR_COUNT)
                {
                    throw std::invalid_argument("Invalid input");
                }

                //Set flavor
                flavor = static_cast<Flavor>(userChoice);

                validInput = true;
            }
            catch(...)
            {
                std::cin.clear();
                std::cout << "\n\nInvalid input" << std::endl;
                waitForEnter();
            }
        } 
    }  
}

void Cone::display() const
{
    std::cout << "Cone: " << std::endl;
    std::cout << "   |- Cone Type: " << coneTypeToString(coneType) << std::endl;
    std::cout << "   |- Scoops: " << scoops << std::endl;
    std::cout << "   |- Flavor: " << flavorToString(flavor) << std::endl;
    std::cout << "   |- Price: " << getPrice() << std::endl;
}

void Cone::applyDiscount()
{
    if (!discountApplied)
    {
        price -= price * CONE_DISCOUNT_PERCENT;
        discountApplied = true;
    }
}

std::string Cone::coneTypeToString(ConeType type) const
{
    switch(type)
    {
        case SUGAR:
            return "Sugar";
        case WAFFLE:
            return "Waffle";
        case BOWL:
            return "Bowl";
        default:
            return "Invalid";
    }
}
//********************************************************************************
//END Cone
//********************************************************************************


//********************************************************************************
//BEGIN Sandwich
//********************************************************************************
Sandwich::Sandwich() : IceCreamDessert(), size(SMALL), cookie(CHOCOLATE_CHIP)
{
}

Sandwich::~Sandwich()
{
}

void Sandwich::customize(bool suppriseMe)
{
    bool validInput = false; //Flag to determine if input is valid
    std::string userInput; //User input
    int userChoice; //User choice
    int diceRoll; //Random number for supprise me

    //If supprise me is selected, randomly generate the sandwich
    if(suppriseMe)
    {
        //Generate random size
        diceRoll = generateRandomInt(SMALL, SIZE_COUNT - 1);
        size = static_cast<Size>(diceRoll);

        //Generate random cookie type
        diceRoll = generateRandomInt(CHOCOLATE_CHIP, COOKIE_COUNT - 1);
        cookie = static_cast<CookieType>(diceRoll);

        //Generate random flavor
        diceRoll = generateRandomInt(VANILLA, FLAVOR_COUNT - 1);
        flavor = static_cast<Flavor>(diceRoll);
    }
    else //Customize sandwich
    {
        //Get participant type
        while (!validInput)
        {
            clearScreen();

            std::cout << "Pick the size of the sandwich: " << std::endl;
            std::cout << "0. Small" << std::endl;
            std::cout << "1. Medium" << std::endl;
            std::cout << "2. Large" << std::endl;
            
            try
            {
                //Get user input
                std::cin >> userInput;

                //Validate user input
                userChoice = validateNumOption(userInput, 1);
                if (userChoice == INT_MIN || userChoice < SMALL || userChoice >= SIZE_COUNT)
                {
                    throw std::invalid_argument("Invalid input");
                }
                
                //Set size
                size = static_cast<Size>(userChoice);

                clearScreen();

                //Get cookie type
                std::cout << "Pick the type of cookie: " << std::endl;
                std::cout << "0. Chocolate Chip" << std::endl;
                std::cout << "1. Oatmeal Raisin" << std::endl;
                std::cout << "2. Peanut Butter" << std::endl;
                
                //Get user input
                std::cin >> userInput;

                //Validate user input
                userChoice = validateNumOption(userInput, 1);
                if (userChoice == INT_MIN || userChoice < CHOCOLATE_CHIP || userChoice >= COOKIE_COUNT)
                {
                    throw std::invalid_argument("Invalid input");
                }
                
                //Set cookie type
                cookie = static_cast<CookieType>(userChoice);

                clearScreen();

                //Get flavor
                std::cout << "Pick the flavor: " << std::endl;
                std::cout << "0. Vanilla" << std::endl;
                std::cout << "1. Chocolate" << std::endl;
                std::cout << "2. Strawberry" << std::endl;
                std::cout << "3. Cookies and Cream" << std::endl;

                //Get user input
                std::cin >> userInput;

                //Validate user input
                userChoice = validateNumOption(userInput, 1);
                if (userChoice == INT_MIN || userChoice < VANILLA || userChoice >= FLAVOR_COUNT)
                {
                    throw std::invalid_argument("Invalid input");
                }

                //Set flavor
                flavor = static_cast<Flavor>(userChoice);

                validInput = true;
            }
            catch(...)
            {
                std::cin.clear();
                std::cout << "\n\nInvalid input" << std::endl;
                waitForEnter();
            }
        } 
    }
}

void Sandwich::display() const
{
    std::cout << "Sandwich: " << std::endl;
    std::cout << "   |- Size: " << sizeToString(size) << std::endl;
    std::cout << "   |- Cookie: " << cookieTypeToString(cookie) << std::endl;
    std::cout << "   |- Flavor: " << flavorToString(flavor) << std::endl;
    std::cout << "   |- Price: " << getPrice() << std::endl;
}

void Sandwich::applyDiscount()
{
    if (!discountApplied)
    {
        price -= price * SANDWICH_DISCOUNT_PERCENT;
        discountApplied = true;
    }
}

std::string Sandwich::sizeToString(Size size) const
{
    switch(size)
    {
        case SMALL:
            return "Small";
        case MED:
            return "Medium";
        case LARGE:
            return "Large";
        default:
            return "Invalid";
    }
}

std::string Sandwich::cookieTypeToString(CookieType type) const
{
    switch(type)
    {
        case CHOCOLATE_CHIP:
            return "Chocolate Chip";
        case OATMEAL:
            return "Oatmeal Raisin";
        case PEANUT_BUTTER:
            return "Peanut Butter";
        default:
            return "Invalid";
    }
}
//********************************************************************************
//END Sandwich
//********************************************************************************

//********************************************************************************
//BEGIN Cake
//********************************************************************************
Cake::Cake() : IceCreamDessert(), message(""), filling(NONE)
{
}

Cake::~Cake()
{
}

void Cake::customize(bool suppriseMe)
{
    bool validInput = false; //Flag to determine if input is valid
    std::string userInput; //User input
    int userChoice; //User choice
    int diceRoll; //Random number for supprise me

    //If supprise me is selected, randomly generate the cake
    if(suppriseMe)
    {
        //Generate random message
        message = CAKE_DEFAULT_MESSAGE;

        //Generate random filling
        diceRoll = generateRandomInt(NONE, FILLING_COUNT - 1);
        filling = static_cast<Filling>(diceRoll);

        //Generate random flavor
        diceRoll = generateRandomInt(VANILLA, FLAVOR_COUNT - 1);
        flavor = static_cast<Flavor>(diceRoll);
    }
    else //Customize cake
    {
        //Get participant type
        while (!validInput)
        {
            clearScreen();

            std::cout << "Pick the type of filling: " << std::endl;
            std::cout << "0. None" << std::endl;
            std::cout << "1. Cherry" << std::endl;
            std::cout << "2. Fudge" << std::endl;
            std::cout << "3. Caramel" << std::endl;
            
            try
            {
                //Get user input
                std::cin >> userInput;

                //Validate user input
                userChoice = validateNumOption(userInput, 1);
                if (userChoice == INT_MIN || userChoice < NONE || userChoice >= FILLING_COUNT)
                {
                    throw std::invalid_argument("Invalid input");
                }
                
                //Set filling
                filling = static_cast<Filling>(userChoice);

                clearScreen();

                //Get flavor
                std::cout << "Pick the flavor: " << std::endl;
                std::cout << "0. Vanilla" << std::endl;
                std::cout << "1. Chocolate" << std::endl;
                std::cout << "2. Strawberry" << std::endl;
                std::cout << "3. Cookies and Cream" << std::endl;

                //Get user input
                std::cin >> userInput;

                //Validate user input
                userChoice = validateNumOption(userInput, 1);
                if (userChoice == INT_MIN || userChoice < VANILLA || userChoice >= FLAVOR_COUNT)
                {
                    throw std::invalid_argument("Invalid input");
                }

                //Set flavor
                flavor = static_cast<Flavor>(userChoice);

                validInput = true;
            }
            catch(...)
            {
                std::cin.clear();
                std::cout << "\n\nInvalid input" << std::endl;
                waitForEnter();
            }
        } 
    }
}

void Cake::display() const
{
    std::cout << "Cake: " << std::endl;
    std::cout << "   |- Message: " << message << std::endl;
    std::cout << "   |- Filling: " << fillingToString(filling) << std::endl;
    std::cout << "   |- Flavor: " << flavorToString(flavor) << std::endl;
    std::cout << "   |- Price: " << getPrice() << std::endl;
}

void Cake::applyDiscount()
{
    if (!discountApplied)
    {
        price -= price * CAKE_DISCOUNT_PERCENT;
        discountApplied = true;
    }
}

void Cake::writeMessage(const std::string & newMessage)
{
    message = newMessage;
}

std::string Cake::fillingToString(Filling filling) const
{
    switch(filling)
    {
        case NONE:
            return "None";
        case CHERRY:
            return "Cherry";
        case FUDGE:
            return "Fudge";
        case CARAMEL:
            return "Caramel";
        default:
            return "Invalid";
    }
}
//********************************************************************************
//END Cake
//********************************************************************************