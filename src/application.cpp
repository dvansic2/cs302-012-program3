/*
 * Name: Demetri Van Sickle
 * Date: 05/19/2024
 * Class: CS302
 * Assignment: Program 3
 * Description:
 *      Contains the class implementations for the DessertManager
 */
#include <iostream>
#include <climits>
#include <cassert>
#include "application.h"
#include "displayHelpers.h"

DessertManager::DessertManager()
{
}

DessertManager::~DessertManager()
{
}

void DessertManager::start()
{   
    bool validInput = false; //Flag to determine if input is valid
    bool userExit = false; //Flag to determine if user wants to exit
    int optionChoiceNum = -1;  //Number of the option chosen
    std::string usrInput;   //General input string

    while(!userExit)
    {   
        //Reset variables
        optionChoiceNum = -1;
        validInput = false;

        //Get menu option
        while(!validInput)
        {
            clearScreen();

            //Display main menu (ORDER MUST MATCH ENUM)
            std::cout << "Welcome to the dessert manager!\n" << std::endl;
            std::cout << "Please select an option:" << std::endl;
            std::cout << "0. Add Dessert" << std::endl;
            std::cout << "1. Get Total Profit by flavor" << std::endl;
            std::cout << "2. Display bought desserts by flavor" << std::endl;
            std::cout << "3. Remove all entries" << std::endl;
            std::cout << "4. Exit" << std::endl;
            
            try
            {
                //Get user input
                std::cin >> usrInput;

                //Validate input
                optionChoiceNum = validateNumOption(usrInput, 1);
                if (optionChoiceNum == INT_MIN || optionChoiceNum < ADD_DESSERT || optionChoiceNum >= OPTION_COUNT)
                {
                    throw std::invalid_argument("Invalid input");
                }

                validInput = true;
            }
            catch(...)
            {
                std::cin.clear();
                std::cout << "\n\nInvalid input" << std::endl;
                waitForEnter();
            }
        } 

        //Perform action based on menu option
        switch(optionChoiceNum)
        {
        case ADD_DESSERT:
            addDessert();
            break;

        case GET_TOTAL_PROFIT:
            clearScreen();
            
            for(int flavor = 0; flavor < FLAVOR_COUNT; ++flavor)
            {
                std::cout << "Total profit for " << flavorToString(static_cast<Flavor>(flavor)) << ": $" << getTotalProfitOfFlavor(static_cast<Flavor>(flavor)) << std::endl;
            }

            waitForEnter();
            break;

        case DISPLAY:
            desserts.display();
            waitForEnter();
            break;

        case CLEAR_LIST:
            desserts.removeAll();
            break;

        case EXIT:
            userExit = true;
            break;
        default:
            //This will never be hit as we validate above, assert for debug
            std::cout << "got: " << optionChoiceNum << std::endl;
            assert(false);
        }

    }
}

std::string DessertManager::flavorToString(Flavor flavor) const
{
    switch(flavor)
    {
        case VANILLA:
            return "Vanilla";
        case CHOCOLATE:
            return "Chocolate";
        case STRAWBERRY:
            return "Strawberry";
        case COOKIES_AND_CREAM:
            return "Cookies and Cream";
        default:
            return "Invalid";
    }
}

void DessertManager::addDessert()
{
    bool validInput = false; //Flag to determine if input is valid
    bool userExit = false;  //Flag to determine if user wants to exit
    bool randomDessertOptions = false; //Flag to determine if the dessert options should be random
    bool isSale = false; //Flag to determine if the dessert is on sale
    int numDesserts;  //Input from the user
    int optionChoiceNum = -1;  //Number of the option chosen
    int yesNoChoiceNum = -1;  //Number of the option chosen
    std::string usrInput; //General input string
    std::shared_ptr<IceCreamDessert> dessertShrdPtr; //Pointer to the dessert to add

    //Get number of desserts to add
    while(!validInput)
    {
        clearScreen();

        //Display prompt
        std::cout << "Enter the number of desserts to add: ";
        
        try
        {
            //Get user input
            std::cin >> numDesserts;

            //Validate input
            if (numDesserts < 1)
            {
                throw std::invalid_argument("Invalid input");
            }

            //Ask if they want the desserts to be random
            std::cout << "Would you like the dessert customizations to be random? " << std::endl;
            std::cout << "0. No" << std::endl;
            std::cout << "1. Yes" << std::endl;
            
            std::cin >> usrInput;

            //Validate input
            yesNoChoiceNum = validateNumOption(usrInput, 1);
            if (yesNoChoiceNum == INT_MIN || yesNoChoiceNum < 0 || yesNoChoiceNum > 1)
            {
                throw std::invalid_argument("Invalid input");
            }
            
            //Set the flag
            randomDessertOptions = yesNoChoiceNum;

            validInput = true;
        }
        catch(...)
        {
            std::cin.clear();
            std::cout << "\n\nInvalid input" << std::endl;
            waitForEnter();
        }
    } 

    //Get the number of desserts the user requested
    for(int i = 0; i < numDesserts && !userExit; i++)
    {
        validInput = false;

        //Get dessert to add
        while(!validInput)
        {
            clearScreen();

            std::cout << "Please select an option:" << std::endl;
            std::cout << "0. Ice Cream Cone" << std::endl;
            std::cout << "1. Ice Cream Sandwich" << std::endl;
            std::cout << "2. Ice Cream Cake" << std::endl;
            std::cout << "3. Exit" << std::endl;
            
            try
            {
                //Get user input
                std::cin >> usrInput;

                //Validate input
                optionChoiceNum = validateNumOption(usrInput, 1);
                if (optionChoiceNum == INT_MIN || optionChoiceNum < CONE || optionChoiceNum >= FLAVOR_COUNT)
                {
                    throw std::invalid_argument("Invalid input");
                }

                clearScreen();

                //Dont enter if the user wants to exit
                if(optionChoiceNum != 3)
                {
                    //Get if the dessert is on sale
                    std::cout << "Is the dessert on sale? " << std::endl;
                    std::cout << "0. No" << std::endl;
                    std::cout << "1. Yes" << std::endl;

                    std::cin >> usrInput;

                    //Validate input
                    yesNoChoiceNum = validateNumOption(usrInput, 1);
                    if (yesNoChoiceNum == INT_MIN || yesNoChoiceNum < 0 || yesNoChoiceNum > 1)
                    {
                        throw std::invalid_argument("Invalid input");
                    }

                    isSale = yesNoChoiceNum;
                }

                validInput = true;
            }
            catch(...)
            {
                std::cin.clear();
                std::cout << "\n\nInvalid input" << std::endl;
                waitForEnter();
            }
        } 

        //Add selected dessert
        switch(optionChoiceNum)
        {
        case CONE:
            dessertShrdPtr = std::make_shared<Cone>();
            break;

        case SANDWICH:
            dessertShrdPtr = std::make_shared<Sandwich>();
            break;

        case CAKE:
            dessertShrdPtr = std::make_shared<Cake>();
            break;

        case 3:
            userExit = true;
            break;
        
        default:
            //This will never be hit as we validate above, assert for debug
            std::cout << "got: " << optionChoiceNum << std::endl;
            assert(false);
            break;
        }

        //Add dessert if the user did not exit
        if(!userExit)
        {
            //Customize the dessert
            if(randomDessertOptions)
            {
                dessertShrdPtr->customize(true);
            }
            else
            {
                dessertShrdPtr->customize(false);

                //Try to write a message if it is a cake
                Cake * cake = dynamic_cast<Cake *>(dessertShrdPtr.get());
                if (cake != nullptr)
                {
                    clearScreen();

                    //Ask user for message
                    std::cout << "Enter the message to write on the cake: ";
                    std::cin >> usrInput;

                    cake->writeMessage(usrInput);
                    std::cin.ignore(INT_MAX, '\n');
                }
            }

            //Apply sale if needed
            if(isSale)
            {
                dessertShrdPtr->applyDiscount();
            }

            //Add dessert to the list
            desserts.insert(std::move(dessertShrdPtr));
        }
       
    }
    
}

float DessertManager::getTotalProfitOfFlavor(Flavor flavorToFind) const
{
    keyListshrdPtr flavorList;
    float totalProfit = 0;

    flavorList = desserts.getDessertByFlavor(flavorToFind);

    //If the flavor is found, add the profit
    if(flavorList != nullptr)
    {
        for(std::shared_ptr<IceCreamDessert> dessert: *flavorList)
        {
            totalProfit += dessert->getPrice();
        }
    }
    
    return totalProfit;
}


