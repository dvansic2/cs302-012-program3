// /*
//  * Name: Demetri Van Sickle
//  * Date: 05/14/2024
//  * Class: CS302
//  * Assignment: Program 3
//  * Description:
//  *      NOT FOR GRADING - This is a test file to test the functionality of the code
//  */
// #include <iostream>
// #include <memory>
// #include "iceCream.h"
// #include "234Tree.h"
// #include "application.h"

// int main()
// {
//     // IceCreamDessert * dessert = new Cone();

//     // std::cout << "Price: " << dessert->getPrice() << std::endl;

//     // dessert->customize(true);

//     // dessert->display();

//     // dessert->applyDiscount();

//     // dessert->display();

//     // // delete dessert;

//     // // dessert = new Sandwich();

//     // // std::cout << "Price: " << dessert->getPrice() << std::endl;

//     // // dessert->customize(false);

//     // // dessert->display();

//     // // dessert->applyDiscount();

//     // // dessert->display();

//     // dessert = new Cake();

//     // std::cout << "Price: " << dessert->getPrice() << std::endl;

//     // dessert->customize(true);

//     // dessert->display();

//     // dessert->applyDiscount();

//     // Cake * cake = dynamic_cast<Cake *>(dessert);
//     // if (cake != nullptr)
//     // {
//     //     cake->writeMessage("Happy Birthday TO YOU!");
//     // }
//     // else
//     // {
//     //     std::cout << "Not a cake" << std::endl;
//     // }

//     // dessert->display();

//     // IceCreamDessert * dessert2[] = {new Cone(), new Sandwich(), new Cake()};

//     // for (int i = 0; i < 3; ++i)
//     // {
//     //     dessert2[i]->customize(true);
//     //     dessert2[i]->display();
//     //     dessert2[i]->applyDiscount();
//     //     dessert2[i]->display();
//     // }

//     // for (int i = 0; i < 3; ++i)
//     // {
//     //     dessert2[i]->display();
//     // }
    
//     // for(int i = 0; i < 3; ++i)
//     // {
//     //     delete dessert2[i];
//     // }

// //-----------------------------

//     Node anotherNode;
//     int keySpot;

//     std::shared_ptr<IceCreamDessert> ptr = std::make_shared<Cone>();
//     std::shared_ptr<IceCreamDessert> ptr2 = std::make_shared<Cake>();
//     std::shared_ptr<IceCreamDessert> ptr3 = std::make_shared<Sandwich>();

//     keyListshrdPtr DupKeyList = std::make_shared<std::list< std::shared_ptr<IceCreamDessert> > >();
//     keyListshrdPtr DupKeyList2 = std::make_shared<std::list< std::shared_ptr<IceCreamDessert> > >();
//     keyListshrdPtr DupKeyList3 = std::make_shared<std::list< std::shared_ptr<IceCreamDessert> > >();

//     std::shared_ptr<Node> nodePtr = std::make_shared<Node>();
//     std::shared_ptr<Node> nodePtr2 = std::make_shared<Node>();
//     std::shared_ptr<Node> nodePtr3 = std::make_shared<Node>();

//     ptr->customize(true);
//     ptr2->customize(true);
//     ptr3->customize(true);

//     DupKeyList->emplace_front(std::move(ptr));
//     DupKeyList2->emplace_front(std::move(ptr2));
//     DupKeyList3->emplace_front(std::move(ptr3));

//     anotherNode.addKey(DupKeyList, keySpot);
//     anotherNode.addKey(DupKeyList2, keySpot);
//     anotherNode.addKey(DupKeyList3, keySpot);

//     anotherNode.addChild(std::move(nodePtr), 0);
//     anotherNode.addChild(std::move(nodePtr2), 1);
//     anotherNode.addChild(std::move(nodePtr3), 2);

//     anotherNode.display();

//     //see if node is full
//     std::cout << "\n\nIs full: " << anotherNode.isFull() << std::endl;

    // Node anotherNode2 = anotherNode;

    // anotherNode2.display();


// //-------------------------------------

//     ttfTree tree;

//     std::shared_ptr<IceCreamDessert> ptr4 = std::make_shared<Cone>();
//     std::shared_ptr<IceCreamDessert> ptr5 = std::make_shared<Cake>();
//     std::shared_ptr<IceCreamDessert> ptr6 = std::make_shared<Sandwich>();
//     std::shared_ptr<IceCreamDessert> ptr7 = std::make_shared<Sandwich>();
//     std::shared_ptr<IceCreamDessert> ptr8 = std::make_shared<Cone>();
//     std::shared_ptr<IceCreamDessert> ptr9 = std::make_shared<Sandwich>();
//     std::shared_ptr<IceCreamDessert> ptr10 = std::make_shared<Cone>();

//     ptr4->customize(true);
//     ptr5->customize(true);
//     ptr6->customize(true);
//     ptr7->customize(true);
//     ptr8->customize(true);
//     ptr9->customize(true);
//     ptr10->customize(true);

//     ptr4->display();
//     ptr5->display();
//     ptr6->display();
//     ptr7->display();
//     ptr8->display();
//     ptr9->display();
//     ptr10->display();

//     tree.insert(std::move(ptr4));
//     tree.insert(std::move(ptr5));
//     tree.insert(std::move(ptr6));
//     tree.insert(std::move(ptr7));
//     tree.insert(std::move(ptr8));
//     tree.insert(std::move(ptr9));
//     tree.insert(std::move(ptr10));

//     tree.display();

//     keyListshrdPtr foundList = tree.getDessertByFlavor(VANILLA);

//     if(foundList != nullptr)
//     {   
//         Cake * ptr = dynamic_cast<Cake *>(foundList->front().get());

//         if(ptr != nullptr)
//         {
//             ptr->writeMessage("NEW MESSASGE");
//         }

//         std::cout << "\n\n\n";
//         foundList->front()->display();
//     }


    // ttfTree tree2 = tree;

    // tree2.display();
    
    // tree.removeAll();

    // tree.display();

//     //-----------------------------


//     DessertManager app;

//     app.start();

//     return 0;
// }